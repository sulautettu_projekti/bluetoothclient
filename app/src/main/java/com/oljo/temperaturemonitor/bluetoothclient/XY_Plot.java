package com.oljo.temperaturemonitor.bluetoothclient;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.renderscript.ScriptGroup;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.androidplot.Plot;
import com.androidplot.util.PixelUtils;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.PointLabelFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.androidplot.xy.XYStepMode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class XY_Plot extends AppCompatActivity {


    private XYPlotZoomPan dynamicPlot;
    private SimpleXYSeries tempSeries = null;
    private HashMap<SimpleXYSeries, Boolean> seriesList = new HashMap<>();
    private PointF minXY;
    private PointF maxXY;

    final Messenger mMessenger = new Messenger(new IncomingHandler());
    Messenger mService = null;
    boolean mIsBound;

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MessengerService.MSG_GET_TEMP_DATA_RESP:
                    drawJsonToXY(msg.getData());
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("JSONARRAY", "x");
    }


    private void drawJsonToXY(Bundle b) {
        String s = b.getString("JSONARRAY");
        if(s != null) {
            try {
                JSONArray jsonArray = new JSONArray(s);
                for(SimpleXYSeries tmpSeries : seriesList.keySet()) {
                    while (tmpSeries.size() > 0) {
                        tmpSeries.removeFirst();
                    }
                }
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    Long time = jsonObject.getLong("timestamp");
                    Double temp = jsonObject.getDouble("temperature");
                    String sId = new Integer(jsonObject.getInt("sensorId")).toString();
                    if(!containsSeries(sId)) {
                        addNewSeries(sId);
                        addSeriesToPlot();
                    }
                    tempSeries = getSeriesByTitle(sId);
                    if (temp != 999) {
                        tempSeries.addLast(time, temp);
                    }
                }
                dynamicPlot.calculateMinMaxVals();


                minXY = new PointF(dynamicPlot.getCalculatedMinX().floatValue(), dynamicPlot.getCalculatedMinY().floatValue());
                maxXY = new PointF(dynamicPlot.getCalculatedMaxX().floatValue(), dynamicPlot.getCalculatedMaxY().floatValue());
                dynamicPlot.setDomainBoundaries((minXY.x), (maxXY.x), BoundaryMode.AUTO);
                dynamicPlot.setRangeBoundaries(minXY.y, maxXY.y, BoundaryMode.AUTO);
                dynamicPlot.redraw();

            } catch (JSONException e) {
                Log.e("JSON Exception", e.getLocalizedMessage());
            }
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            mService = new Messenger(service);
            try {
                Message msg = Message.obtain(null,
                        MessengerService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);
                Message msg1 = Message.obtain(null,
                        MessengerService.MSG_GET_TEMP_DATA_REQ);
                msg1.replyTo = mMessenger;
                mService.send(msg1);
            } catch (RemoteException e) {
                Log.e("Service Crash", e.getLocalizedMessage());
            }
        }

        public void onServiceDisconnected(ComponentName className) {mService = null;}
    };

    private void addNewSeries(String title) {
        SimpleXYSeries xySeries = new SimpleXYSeries(title);
        seriesList.put(xySeries, false);
    }

    private SimpleXYSeries getSeriesByTitle(String title) {
        for(XYSeries tmpSer : seriesList.keySet()) {
            if(tmpSer.getTitle().equals(title)){
                return (SimpleXYSeries)tmpSer;
            }
        }
        return null;
    }

    private boolean containsSeries(String title) {
        for(XYSeries tmpSer : seriesList.keySet()) {
            if(tmpSer.getTitle().equals(title)){
                return true;
            }
        }
        return false;
    }

    private void addSeriesToPlot() {
        for(XYSeries xyTemp : seriesList.keySet()) {
            if(!seriesList.get(xyTemp)) {
                //TODO check that format is saved when coming back to plot
                //TODO Make sure that color is distinguishable from background
                Integer[] randomRGB = {new Random().nextInt(100), new Random().nextInt(200), new Random().nextInt(100)};
                dynamicPlot.addSeries(xyTemp, new LineAndPointFormatter(
                        Color.rgb(randomRGB[0],
                                randomRGB[1],
                                (randomRGB[2])), null, null, null));
                seriesList.put((SimpleXYSeries)xyTemp, true);
            }
        }
    }

    void doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        bindService(new Intent(XY_Plot.this,
                MessengerService.class), mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void unBindService() {
        unbindService(mConnection);
        mIsBound = false;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        // android boilerplate stuff
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_xy_plot);

        if(!mIsBound) {
            doBindService();
        }

        // get handles to our View defined in layout.xml:
        dynamicPlot = (XYPlotZoomPan) findViewById(R.id.mySimpleXYPlot);
        dynamicPlot.setZoomEnabled(true);
        dynamicPlot.getGraphWidget().setDomainValueFormat(new DecimalFormat("0"));
        dynamicPlot.setDomainStep(XYStepMode.SUBDIVIDE, 5);
        dynamicPlot.setZoomVertically(false);
        dynamicPlot.setZoomHorizontally(true);
        dynamicPlot.setDomainValueFormat(new Format() {

            // create a simple date format that draws on the year portion of our timestamp.
            // see http://download.oracle.com/javase/1.4.2/docs/api/java/text/SimpleDateFormat.html
            // for a full description of SimpleDateFormat.
            private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

            @Override
            public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {

                // because our timestamps are in seconds and SimpleDateFormat expects milliseconds
                // we multiply our timestamp by 1000:
                long timestamp = ((Number) obj).longValue() * 1000;
                Date date = new Date(timestamp);
                return dateFormat.format(date, toAppendTo, pos);
            }

            @Override
            public Object parseObject(String source, ParsePosition pos) {
                return null;

            }
        });

        dynamicPlot.setRangeStepMode(XYStepMode.SUBDIVIDE);
        dynamicPlot.setRangeStepValue(10);

        dynamicPlot.setRangeValueFormat(new DecimalFormat("##.##"));

        // uncomment this line to freeze the range boundaries:
        dynamicPlot.setRangeBoundaries(10, 25, BoundaryMode.AUTO);

        // create a dash effect for domain and range grid lines:
        DashPathEffect dashFx = new DashPathEffect(
                new float[] {PixelUtils.dpToPix(3), PixelUtils.dpToPix(3)}, 0);
        dynamicPlot.getGraphWidget().getDomainGridLinePaint().setPathEffect(dashFx);
        dynamicPlot.getGraphWidget().getRangeGridLinePaint().setPathEffect(dashFx);
        dynamicPlot.calculateMinMaxVals();
        minXY=new PointF(dynamicPlot.getCalculatedMinX().floatValue(),dynamicPlot.getCalculatedMinY().floatValue());
        maxXY=new PointF(dynamicPlot.getCalculatedMaxX().floatValue(),dynamicPlot.getCalculatedMaxY().floatValue());
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            Message msg = Message.obtain(null,
                    MessengerService.MSG_UNREGISTER_CLIENT);
            msg.replyTo = mMessenger;
            mService.send(msg);
        } catch (RemoteException e) {
            Log.e("Service Crash", e.getLocalizedMessage());
        }
        try {
            unBindService();
        } catch (IllegalArgumentException e) {

        }
    }
}