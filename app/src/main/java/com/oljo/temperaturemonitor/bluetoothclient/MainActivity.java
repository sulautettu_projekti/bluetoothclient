package com.oljo.temperaturemonitor.bluetoothclient;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import com.oljo.temperaturemonitor.bluetoothclient.Settings;
import android.app.Notification;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.hardware.display.DisplayManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    private TextView tmpTextView;
    private JSONArray jsonTempArray = new JSONArray();
    private String jsonData;
    private Button connectBtBtn;
    private ListView tempView;
    private Boolean mBtState = false;
    public static ConnectThread connect;
    public static BluetoothDevice useDevice = null;
    private Long tempAlertTime=0l, sensorAlertTime=0l;
    private BluetoothAdapter adapter;
    private Bundle b;
    private HashMap<Integer, Double> sensorTempMap = new HashMap<>();
    private ArrayAdapter<String> listAdapter;
    private ArrayAdapter<String> btDevlistAdapter;

    final int btData = 0;
    final int btState = 1;

    public static final int REQUEST_ENABLE_BT = 9;
    public static int MAX_TEMP_ENTRIES = 500;
    public static Uri ALERT_SOUND_URI;
    public static double ALERT_TEMP_MAX = 25.0;
    public static double ALERT_TEMP_MIN = 10.0;
    public static boolean FLAG_BT_DEV_CHANGE = false;
    public static boolean REMEMBER_BT_DEVICE = false;
    public static int ALERT_TIME = 1; //Time between alerts in minutes.
    public static final boolean DEBUG = false;
    public static boolean VIBRATE_ALERT = true;
    public static boolean SOUND_ALERT = true;
    public static String LAST_BT_ADDRESS;

    public BroadcastReceiver wakeLockReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            }
        }
    };

    public void wakeupScreen() {
        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);

        DisplayManager dm = (DisplayManager) getSystemService(Context.DISPLAY_SERVICE);
        for (Display display : dm.getDisplays()) {
            if (display.getState() == Display.STATE_OFF) {
                PowerManager.WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
                wakeLock.acquire();
                wakeLock.release();
            }
        }
    }

    private final AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            setSensorTempTxt();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    public void setSensorTempTxt() {
        String tmp = new String();
        SortedMap<Integer, Double> tmpMap = new TreeMap<>(sensorTempMap);

        int n=0;
        Double tmp_temp=0.0, avg_temp;
        for (Double temp : tmpMap.values()) {
            tmp_temp += temp;
            n++;
        }
        avg_temp = tmp_temp/n;
        String avg_temp_s = avg_temp.toString();
        int integerPlaces = avg_temp_s.indexOf('.');
        int decimalPlaces = avg_temp_s.length() - integerPlaces - 1;
        if(decimalPlaces > 2) {
            avg_temp_s = avg_temp_s.substring(0, integerPlaces+3);
        }
        tmp = tmp.concat("Average: " + avg_temp_s + "°C" + "\n");

        for (Integer iKey : tmpMap.keySet()) {
            tmp = tmp.concat(iKey.toString() + ": " + tmpMap.get(iKey).toString() + "°C" + "\n");
        }

        tmpTextView.setText(tmp);
    }

    final Messenger mMessenger = new Messenger(new IncomingHandler());
    Messenger mService = null;
    boolean mIsBound;

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            mService = new Messenger(service);
            try {
                Message msg = Message.obtain(null,
                        MessengerService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);
            } catch (RemoteException e) {
                Log.e("Service Crash", e.getLocalizedMessage());
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            mService = null;
        }
    };

    void doBindService() {
        bindService(new Intent(MainActivity.this,
                MessengerService.class), mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    private void sendServiceMsg(Message msgIn) {
        Message msg;
        msg = msgIn;
        try {
            mService.send(msg);
        } catch (RemoteException e) {
            Log.e("RemoteException", e.getLocalizedMessage());
        }
    }


    public final Handler bluetoothIn = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);

            if (msg.what == btData) {
                String s = msg.getData().getString("JSONARRAY");
                jsonStringParser(s);
            }
            else if(msg.what == btState) {
                if(!msg.getData().getBoolean("STATUS")) {
                    mBtState = msg.getData().getBoolean("STATUS");
                    Toast.makeText(MainActivity.this, "Connection failed!", Toast.LENGTH_SHORT).show();
                    connectBtBtn.setText("Connect bluetooth");
                    connectBtBtn.setClickable(true);
                }
            }
        }

    };

    public void jsonStringParser(String sIn) {
        jsonData = sIn;
                SimpleDateFormat dt = new SimpleDateFormat("HH:mm:ss");
                try {
                    JSONArray jsonArray = new JSONArray(sIn);
                    for(int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        Integer sensorId;

                        if(DEBUG) {
                            sensorId = (new Random()).nextInt(6);
                            jsonObject.remove("sensorId");
                            jsonObject.put("sensorId", sensorId);
                        }
                        else {
                            sensorId = jsonObject.getInt("sensorId");
                        }
                        jsonTempArray.put(jsonObject);
                        Long time = jsonObject.getLong("timestamp");
                        Double temp = jsonObject.getDouble("temperature");
                        Date date = new Date(time * 1000);
                        listAdapter.clear();

                        if(temp == 999 && (time - sensorAlertTime) > (ALERT_TIME * 60)) {
                            sensorAlertTime = time;
                            listAdapter.add(dt.format(date) + " Sensor Alert!\nSensor: " + sensorId.toString());
                            notifier("Sensor Alert!", dt.format(new Date(time*1000)) + ": Bad reading from sensor : " + sensorId.toString(), 001);
                        }
                        else if((temp > ALERT_TEMP_MAX || temp < ALERT_TEMP_MIN) && temp != 999 && ((time - tempAlertTime) > (ALERT_TIME * 60))) {
                            sensorTempMap.put(sensorId,temp);
                            String notifierHeader;
                            if (temp > ALERT_TEMP_MAX) notifierHeader = "High temperature!";
                            else notifierHeader = "Low temperature!";
                            tempAlertTime = time;
                            listAdapter.add(dt.format(date) + " Temperature Alert!\nSensor: " + sensorId.toString() + "\nTemperature: " + temp.toString() + "°C");
                            notifier(notifierHeader, dt.format(new Date(time * 1000)) + ": Temperature alert! Sensor: " + sensorId.toString() + ": " + temp.toString() + "°C", 002);
                        }
                        if(temp != 999) {
                            sensorTempMap.put(sensorId, temp);
                        }
                    }

                    if (jsonTempArray.length() > MAX_TEMP_ENTRIES) {
                        JSONArray tmp = new JSONArray();
                        for(int i=(jsonTempArray.length() - MAX_TEMP_ENTRIES); i < jsonTempArray.length(); i++) {
                            tmp.put(jsonTempArray.get(i));
                        }
                        jsonTempArray = tmp;
                    }
                    b = new Bundle();
                    b.putString("JSONARRAY", jsonArray.toString());
                    Message msgToService = Message.obtain();
                    msgToService.setData(b);
                    msgToService.what = MessengerService.MSG_SET_VALUE;
                    sendServiceMsg(msgToService);


                    for (int i=0; i < jsonTempArray.length(); i++){

                        JSONObject jsonObject = jsonTempArray.getJSONObject(i);

                        Long time = jsonObject.getLong("timestamp");
                        Double temp = jsonObject.getDouble("temperature");
                        Integer sensorId = jsonObject.getInt("sensorId");
                        Date date = new Date(time * 1000);

                        if(temp != 999 && (temp < ALERT_TEMP_MAX && temp > ALERT_TEMP_MIN)){
                            listAdapter.add(dt.format(date) + "\nSensor: " + sensorId.toString() + "\nTemperature: " + temp.toString() + "°C");
                        }
                        setTempView();
                        setSensorTempTxt();
                    }

                } catch (JSONException eJson) {
                    Log.e("ERROR", eJson.getLocalizedMessage());
                }
    }

    public void notifier(String title, String text, int mNotificationId) {
        wakeupScreen();
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(android.R.drawable.ic_dialog_alert)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setShowWhen(true)
                        .setVisibility(Notification.VISIBILITY_PUBLIC);
        if (SOUND_ALERT) {
            Uri soundUri = ALERT_SOUND_URI;//RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            mBuilder.setSound(soundUri);
        }
        if (VIBRATE_ALERT) {
            long[] pattern = {500l,500l};
            mBuilder.setVibrate(pattern);
        }
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("JSONARRAY", jsonData);
    }

    public void startPlot(View v) {
        Intent intent = new Intent(this, XY_Plot.class);
        startActivity(intent);
    }

    public void connectBt(View v) {
        this.connectBtBtn.setClickable(false);
        if(this.adapter == null) {
            new AlertDialog.Builder(this)
                    .setTitle("No Bluetooth device detected!")
                    .setMessage("No bluetooth devices were found!" +
                            "\nExit application?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            System.exit(0);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            this.connectBtBtn.setClickable(true);
            return;
        }

        if(this.adapter.getBondedDevices() != null) {
            if (!adapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
            else {
                if(useDevice == null) {
                    selectBtDevice();
                }
                else if (FLAG_BT_DEV_CHANGE) {
                    FLAG_BT_DEV_CHANGE = false;
                    selectBtDevice();
                }
                else toggleBluetooth();
            }
        }
    }

    private void selectBtDevice() {
        btDevlistAdapter = new ArrayAdapter<String>(this, R.layout.simplerow);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Bluetooth device:");
        final Set<BluetoothDevice> devices = this.adapter.getBondedDevices();
        final List<BluetoothDevice> btDevList = new ArrayList<BluetoothDevice>();

        if(devices.size() > 0) {
            for (BluetoothDevice device : devices) {
                if(REMEMBER_BT_DEVICE && device.getAddress().equals(LAST_BT_ADDRESS)) {
                    Toast.makeText(this, "Selected lastly used device: " + device.getName(), Toast.LENGTH_SHORT).show();
                    useDevice = device;
                    connect = null;
                    toggleBluetooth();
                    return;
                }
                btDevList.add(device);
                Log.d("debug", device.getName());
                btDevlistAdapter.add(device.getName() + " " + device.getAddress());
            }
            builder.setAdapter(btDevlistAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            btDevlistAdapter.getItem(which);
                            useDevice = btDevList.get(which);
                            connect = null;
                            LAST_BT_ADDRESS = useDevice.getAddress();
                            PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
                                    .edit()
                                    .putString(Settings.LAST_BT_ADDRESS_KEY, LAST_BT_ADDRESS)
                                    .apply();
                            toggleBluetooth();
                        }
                    }
            );
        }
        else {
            builder.setTitle("No paired Bluetooth devices found!")
                    .setMessage("Please pair the bluetooth device you wish to connect.")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
                })
                    .setPositiveButton("Pair device", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS));
                        }
                    });

        }
        AlertDialog dialog = builder.create();
        dialog.show();
        connectBtBtn.setClickable(true);
    }

    private void toggleBluetooth() {
        Message toggleBtMsg = Message.obtain();
        toggleBtMsg.what = ConnectThread.TOGGLE_BT_CONNECTION;
        connectBtBtn.setClickable(false);
        if(mBtState) {
            connectBtBtn.setText("Disconnecting...");
            //connect.toggleConnection();
            connect.getHandler().sendMessage(toggleBtMsg);
        } else {
            if (useDevice == null) {
                Log.d("debug", "no device found");
                mBtState = false;
                connectBtBtn.setClickable(true);
                connectBtBtn.setText("Connect Bluetooth");
            } else {
                connectBtBtn.setText("Connecting...");
                if (connect == null) {
                    connect = new ConnectThread(useDevice, bluetoothIn);
                    connect.start();
                } else  {
                    connect.getHandler().sendMessage(toggleBtMsg);
                };
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if(resultCode == Activity.RESULT_OK) {
                selectBtDevice();
                toggleBluetooth();
            }
        }
    }

    private  void setTempView(){
        this.tempView.smoothScrollToPosition(listAdapter.getCount());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null) {
            jsonData = savedInstanceState.getString("JSONARRAY");
            jsonStringParser(jsonData);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent msgServiceIntent = new Intent(this, MessengerService.class);
        startService(msgServiceIntent);
        /*
        Read preferences
         */
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        ALERT_TEMP_MAX = Double.parseDouble(sharedPreferences.getString(Settings.ALERT_MAX_TEMPERATURE, "3.4"));
        ALERT_TEMP_MIN = Double.parseDouble(sharedPreferences.getString(Settings.ALERT_MIN_TEMPERATURE, "-3.4"));
        MAX_TEMP_ENTRIES = Integer.parseInt(sharedPreferences.getString(Settings.TEMP_HISTORY_SIZE_KEY, "500"));
        SOUND_ALERT = sharedPreferences.getBoolean(Settings.ENABLE_ALERT_SOUND_KEY, true);
        VIBRATE_ALERT = sharedPreferences.getBoolean(Settings.ENABLE_ALERT_VIBRATE_KEY, true);
        ALERT_TIME = Integer.parseInt(sharedPreferences.getString(Settings.TIME_BETWEEN_ALERTS_KEY, "5"));
        ALERT_SOUND_URI = Uri.parse(sharedPreferences.getString(Settings.ALERT_SOUND_KEY, RingtoneManager.getActualDefaultRingtoneUri(this, RingtoneManager.TYPE_NOTIFICATION).toString()));
        REMEMBER_BT_DEVICE = sharedPreferences.getBoolean(Settings.REMEMBER_BT_DEV_KEY, false);
        LAST_BT_ADDRESS = sharedPreferences.getString(Settings.LAST_BT_ADDRESS_KEY, "");

        IntentFilter filterScreenOff = new IntentFilter(Intent.ACTION_SCREEN_OFF);
        IntentFilter filterBtConnected = new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED);
        IntentFilter filterBtDisconnectReq = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        IntentFilter filterBtDisconnected = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        this.registerReceiver(mReceiver, filterBtConnected);
        this.registerReceiver(mReceiver, filterBtDisconnectReq);
        this.registerReceiver(mReceiver, filterBtDisconnected);
        this.registerReceiver(wakeLockReceiver, filterScreenOff);

        this.adapter = BluetoothAdapter.getDefaultAdapter();

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(toolbar);

        this.textView = (TextView) findViewById(R.id.text);
        this.tempView = (ListView) findViewById(R.id.tempList);
        ((RelativeLayout)findViewById(R.id.appid)).setOnTouchListener(touchListener);

        this.connectBtBtn = (Button) findViewById(R.id.btConBtn);
        this.tmpTextView = (TextView) findViewById(R.id.tempTxtView);
        listAdapter = new ArrayAdapter<String>(this, R.layout.simplerow);
        tempView.setAdapter(listAdapter);

        doBindService();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this,
                    com.oljo.temperaturemonitor.bluetoothclient.Settings.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            String action = intent.getAction();
            connectBtBtn.setClickable(false);

            if (action.equals(BluetoothDevice.ACTION_ACL_DISCONNECTED)) {
                Toast.makeText(MainActivity.this, "Disconnected.", Toast.LENGTH_SHORT).show();
                connectBtBtn.setText("Connect bluetooth");
                mBtState = false;
            }
            if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Toast.makeText(MainActivity.this, "Connected!", Toast.LENGTH_SHORT).show();
                connectBtBtn.setText("Disconnect bluetooth");
                mBtState = true;
            }
            connectBtBtn.setClickable(true);
        }
    };


    View.OnTouchListener touchListener = new View.OnTouchListener() {
        private float x1,x2;
        private float y1,y2;
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int action = MotionEventCompat.getActionMasked(event);
            switch (action) {
                case (MotionEvent.ACTION_DOWN):
                    x1 = event.getX();
                    y1 = event.getY();
                    Log.d("d", "Action was DOWN");
                    return true;
                case (MotionEvent.ACTION_MOVE):
                    Log.d("d", "Action was MOVE");
                    return true;
                case (MotionEvent.ACTION_UP):
                    x2 = event.getX();
                    y2 = event.getY();
                    if ((x1 - x2) > 100) {
                        ((Button)findViewById(R.id.btnPlot)).performClick();
                    }
                    Log.d("d", "Action was UP");
                    return true;
                case (MotionEvent.ACTION_CANCEL):
                    Log.d("d", "Action was CANCEL");
                    return true;
                case (MotionEvent.ACTION_OUTSIDE):
                    Log.d("d", "Movement occurred outside bounds " +
                            "of current screen element");
                    return true;
                default:
                    return false;
            }
        }
    };
    }
