package com.oljo.temperaturemonitor.bluetoothclient;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by Inno on 13.11.2015.
 */
public class ConnectThread extends Thread {

    public static final int TOGGLE_BT_CONNECTION = 5;
    private BluetoothSocket mmSocket;
    private ConnectedThread connectedThread;
    private final BluetoothDevice mmDevice;
    Message msg;
    Message msg2;
    private final BluetoothAdapter mBluetoothAdapter;
    private Handler hd;
    private Bundle connectionStatus = new Bundle();

    private final static UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

    private Handler myThreadHandler = new Handler(){
        public void handleMessage(Message msg) {
            if (msg.what == TOGGLE_BT_CONNECTION){
                toggleConnection();
            }
        }
    };

    public ConnectThread(BluetoothDevice device, Handler msgHandler)    {
        this.hd = msgHandler;
        BluetoothSocket tmp = null;

        mmDevice = device;

        try {
            tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
        }
        catch (IOException e)   {
            Log.d("debug", "construct failed");
        }
        mmSocket = tmp;

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public void run()   {
        msg = hd.obtainMessage();
        msg.what = 1;
        mBluetoothAdapter.cancelDiscovery();
        try {
            mmSocket.connect();
            connectionStatus.putBoolean("STATUS", true);
            msg.setData(connectionStatus);
            hd.sendMessage(msg);
            Log.d("debug", "connection succeed");
            connectedThread = new ConnectedThread(mmSocket, hd);
            connectedThread.start();
        }
        catch (IOException connectException)    {
            connectionStatus.putBoolean("STATUS", false);
            Log.d("debug", "connection failed : " + connectException.getMessage());
            try {
                mmSocket.close();
            }
            catch (IOException closeException) {
            }
            msg.setData(connectionStatus);
            hd.sendMessage(msg);
            return;
        }
    }

    public Handler getHandler() {
        return myThreadHandler;
    }

    public void toggleConnection() {
        if (mmSocket.isConnected()) {
            try {
                mmSocket.close();
            } catch (IOException e) {

            }
        } else {
            try {
                mmSocket = mmSocket.getRemoteDevice().createRfcommSocketToServiceRecord(MY_UUID);
                mmSocket.connect();
            } catch (IOException connectException) {
                connectionStatus.putBoolean("STATUS", false);
                Log.d("debug", "connection failed : " + connectException.getMessage());
                try {
                    mmSocket.close();
                }
                catch (IOException closeException) {
                }
                msg2 = hd.obtainMessage();
                msg2.what = 1;
                msg2.setData(connectionStatus);
                hd.sendMessage(msg2);
            }
        }
    }

    public void cancel()    {
        try {
            mmSocket.close();
        } catch (IOException e) {}
    }

}
