package com.oljo.temperaturemonitor.bluetoothclient;

import android.bluetooth.BluetoothSocket;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

/**
 * Created by Inno on 13.11.2015.
 */
public class ConnectedThread extends Thread {

    private final BluetoothSocket mmSocket;
    private final InputStream mmInStream;
    private final OutputStream mmOutStream;
    private Handler hd;



    public ConnectedThread(BluetoothSocket socket, Handler msgHandler)  {
        mmSocket = socket;
        this.hd = msgHandler;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;

        try {
            tmpIn = socket.getInputStream();
            tmpOut = socket.getOutputStream();
        }
        catch (IOException e) {}

        mmInStream = tmpIn;
        mmOutStream = tmpOut;
    }

    @Override
    public void run()   {
        char inByte;
        String s = new String();
        Integer bytes;
        JSONArray jsonArray;
        Bundle timeTemp = new Bundle();
        Bundle jsonBundle = new Bundle();
        Message msg = hd.obtainMessage();
        while (true)    {
            try {
                if((inByte=(char)mmInStream.read())!=']') {
                    s+=inByte;
                    if(s.startsWith("AT")) {
                        s = s.replace("AT", "");
                        Log.d("debug","AT Command received.");
                        mmOutStream.write(("DEVICE: " + Build.BRAND + " " + Build.MODEL).getBytes());
                    }
                }
                else {
                    s+=inByte;
                    try {
                        jsonArray = new JSONArray(s);
                        Log.d("JSON: ", "Json array parsed successfully.");
                        jsonBundle.putString("JSONARRAY", s);

                        Long startTime = jsonArray.getJSONObject(0).getLong("timestamp");
                        Long stopTime = jsonArray.getJSONObject(jsonArray.length() - 1).getLong("timestamp");
                        Date dStartTime = new Date(startTime * 1000);
                        Date dStopTime = new Date(stopTime * 1000);
                        Log.d("JSON: ", "Received data between: " + dStartTime.toString() + " - " + dStopTime.toString());
                        mmOutStream.write(stopTime.toString().getBytes());
                        msg = hd.obtainMessage();
                        msg.setData(jsonBundle);
                        hd.sendMessage(msg);
                    } catch (JSONException e) {
                        Log.e("JSON: ", e.getLocalizedMessage());
                    }
                    s = new String();
                }
            }
            catch (IOException e)   {
                break;
            }
        }
    }

    public void cancel()    {
        try {
            mmSocket.close();
        }
        catch (IOException e) {}
    }

}
