package com.oljo.temperaturemonitor.bluetoothclient;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.text.TextUtils;
import android.view.MenuItem;
import android.support.v4.app.NavUtils;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p/>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class Settings extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static String REMEMBER_BT_DEV_KEY = "Pref_rememberBtDev";
    public static String TEMP_HISTORY_SIZE_KEY = "Pref_tempHistorySize";
    public static String ENABLE_ALERT_SOUND_KEY = "enableAlertSound";
    public static String ENABLE_ALERT_VIBRATE_KEY = "enableAlertVibrate";
    public static String ALERT_SOUND_KEY = "notifications_alert_sound";
    public static String TIME_BETWEEN_ALERTS_KEY = "Pref_time_between_alerts";
    public static String ALERT_MAX_TEMPERATURE = "Pref_alert_max_temperature";
    public static String ALERT_MIN_TEMPERATURE = "Pref_alert_min_temperature";
    public static String LAST_BT_ADDRESS_KEY = "pref_last_bt_address";
    private static List<String> fragments = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
        addPreferencesFromResource(R.xml.preferences);
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).registerOnSharedPreferenceChangeListener(this);
        bindPreferenceSummaryToValue(findPreference(ALERT_SOUND_KEY));
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Show the Up button in the action bar.
            //getActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected boolean isValidFragment(String fragmentName) {
        return fragments.contains(fragmentName);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            // that hierarchy.
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //TODO Add checks to verify validity of settings
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                          String key) {
        if (key.equals(REMEMBER_BT_DEV_KEY)) {
            MainActivity.REMEMBER_BT_DEVICE = sharedPreferences.getBoolean(REMEMBER_BT_DEV_KEY, false);
            if (MainActivity.REMEMBER_BT_DEVICE == false) {
                MainActivity.useDevice = null;
            }
            else {
                MainActivity.FLAG_BT_DEV_CHANGE = true;
            }
        }
        if (key.equals(TEMP_HISTORY_SIZE_KEY)) {
            Integer tempHistorySize = Integer.parseInt(sharedPreferences.getString(TEMP_HISTORY_SIZE_KEY, "500"));
            if (tempHistorySize >= 10 && tempHistorySize <= 1000) {
                MainActivity.MAX_TEMP_ENTRIES = tempHistorySize;
            } else {
                Toast.makeText(Settings.this, "Entry size must be between 10 and 1000", Toast.LENGTH_SHORT).show();
                sharedPreferences.edit().putString(key, Integer.toString(MainActivity.MAX_TEMP_ENTRIES)).apply();
            }
        }
        if (key.equals(ENABLE_ALERT_SOUND_KEY)) {
            MainActivity.SOUND_ALERT = sharedPreferences.getBoolean(ENABLE_ALERT_SOUND_KEY, true);
        }
        if (key.equals(ENABLE_ALERT_VIBRATE_KEY)) {
            MainActivity.VIBRATE_ALERT = sharedPreferences.getBoolean(ENABLE_ALERT_VIBRATE_KEY, true);
        }
        if (key.equals(ALERT_SOUND_KEY)) {
            MainActivity.ALERT_SOUND_URI = Uri.parse(sharedPreferences.getString(ALERT_SOUND_KEY, RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION).toString()));
        }
        if (key.equals(TIME_BETWEEN_ALERTS_KEY)) {
            Integer alertTime = Integer.parseInt(sharedPreferences.getString(TIME_BETWEEN_ALERTS_KEY, "2"));
            if (alertTime >= 1 && alertTime <= 120) {
                MainActivity.ALERT_TIME = alertTime;
            } else {
                Toast.makeText(Settings.this, "Time between alerts must be 1-120 minutes.", Toast.LENGTH_SHORT).show();
            }
        }
        if (key.equals(ALERT_MAX_TEMPERATURE)) {
            Double maxTemp = Double.parseDouble(sharedPreferences.getString(ALERT_MAX_TEMPERATURE, "3.4"));
            if(maxTemp > MainActivity.ALERT_TEMP_MIN) {
                MainActivity.ALERT_TEMP_MAX = maxTemp;
            } else {
                Toast.makeText(Settings.this, "Maximum temperature must be larger than minimum.", Toast.LENGTH_SHORT).show();
                sharedPreferences.edit().putString(key, Double.toString(MainActivity.ALERT_TEMP_MAX)).apply();
            }
        }
        if (key.equals(ALERT_MIN_TEMPERATURE)) {
            Double minTemp = Double.parseDouble(sharedPreferences.getString(ALERT_MIN_TEMPERATURE, "-3.4"));
            if(minTemp < MainActivity.ALERT_TEMP_MAX) {
                MainActivity.ALERT_TEMP_MIN = minTemp;
            } else {
                Toast.makeText(Settings.this, "Minimum temperature must be smaller than maximum.", Toast.LENGTH_SHORT).show();
                sharedPreferences.edit().putString(key, Double.toString(MainActivity.ALERT_TEMP_MIN)).apply();
            }
        }
    }


        /**
         * A preference value change listener that updates the preference's summary
         * to reflect its new value.
         */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference instanceof RingtonePreference) {
                    MainActivity.ALERT_SOUND_URI = Uri.parse(stringValue);
                }
            return true;
        }
    };

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);
    }
}
