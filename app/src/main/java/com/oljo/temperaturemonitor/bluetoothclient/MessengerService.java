package com.oljo.temperaturemonitor.bluetoothclient;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class MessengerService extends Service {
    /** For showing and hiding our notification. */
    NotificationManager mNM;
    /** Keeps track of all current registered clients. */
    ArrayList<Messenger> mClients = new ArrayList<Messenger>();
    /** Holds last value set by a client. */
    Bundle mValue;
    private JSONArray temperatureData = new JSONArray();
    private String jsonTemperatureString = new String();
    private Bundle tempBundle = new Bundle();
    /**
     * Command to the service to register a client, receiving callbacks
     * from the service.  The Message's replyTo field must be a Messenger of
     * the client where callbacks should be sent.
     */
    static final int MSG_REGISTER_CLIENT = 1;

    /**
     * Command to the service to unregister a client, ot stop receiving callbacks
     * from the service.  The Message's replyTo field must be a Messenger of
     * the client as previously given with MSG_REGISTER_CLIENT.
     */
    static final int MSG_UNREGISTER_CLIENT = 2;

    /**
     * Command to service to set a new value.  This can be sent to the
     * service to supply a new value, and will be sent by the service to
     * any registered clients with the new value.
     */
    static final int MSG_SET_VALUE = 3;

    static final int MSG_GET_TEMP_DATA_REQ = 4;
    static final int MSG_GET_TEMP_DATA_RESP = 5;

    /**
     * Handler of incoming messages from clients.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            Message sendMsg;
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClients.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    break;
                case MSG_GET_TEMP_DATA_REQ:
                    for (int i=mClients.size()-1; i>=0; i--) {
                        try {
                            sendMsg = Message.obtain();
                            sendMsg.what = MSG_GET_TEMP_DATA_RESP;
                            tempBundle.putString("JSONARRAY", jsonTemperatureString);
                            sendMsg.setData(tempBundle);
                            mClients.get(i).send(sendMsg);
                        } catch (RemoteException e) {
                            mClients.remove(i);
                        }
                    }
                    break;
                case MSG_SET_VALUE:
                    tempBundle = msg.getData();
                    String s = tempBundle.getString("JSONARRAY");
                    try {
                        JSONArray jsonArray = new JSONArray(s);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            temperatureData.put(jsonObject);
                        }

                        if (temperatureData.length() > MainActivity.MAX_TEMP_ENTRIES) {
                            JSONArray tmp = new JSONArray();
                            for (int i = (temperatureData.length() - MainActivity.MAX_TEMP_ENTRIES); i < temperatureData.length(); i++) {
                                tmp.put(temperatureData.get(i));
                            }
                            temperatureData = tmp;
                        }
                        for (int i=mClients.size()-1; i>=0; i--) {
                            try {
                                sendMsg = Message.obtain();
                                sendMsg.what = MSG_GET_TEMP_DATA_RESP;
                                jsonTemperatureString = temperatureData.toString();
                                tempBundle.putString("JSONARRAY", jsonTemperatureString);
                                sendMsg.setData(tempBundle);
                                mClients.get(i).send(sendMsg);
                            } catch (RemoteException e) {
                                mClients.remove(i);
                            }
                        }
                    } catch (JSONException e) {

                    }

                    /*for (int i=mClients.size()-1; i>=0; i--) {
                        try {
                            sendMsg = Message.obtain();
                            sendMsg.copyFrom(msg);
                            mClients.get(i).send(sendMsg);//(Message.obtain(null,
                                    //MSG_SET_VALUE, mValue, 0));
                        } catch (RemoteException e) {
                            // The client is dead.  Remove it from the list;
                            // we are going through the list from back to front
                            // so this is safe to do inside the loop.
                            mClients.remove(i);
                        }
                    }*/
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());

    @Override
    public void onCreate() {
        mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    public void onDestroy() {
    }

    /**
     * When binding to the service, we return an interface to our messenger
     * for sending messages to the service.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }
}